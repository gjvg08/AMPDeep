'''This transforms the given csv files into a format processible for traintool and vice versa to ampdeep.'''

import os
import pandas as pd


def amp_to_traintool(input_data_dir, output_data_dir):
    for input_file in os.listdir(input_data_dir):
        input_file_path = os.path.join(input_data_dir, input_file)
        data = pd.read_csv(input_file_path)
        data.rename(columns={"text": "sequence", "labels": "class"}, inplace=True)
        data["sequence"] = data["sequence"].str.replace(" ", "")
        # data = data[data["sequence"].str.len() <= 25]
        data.to_csv(output_data_dir + input_file, sep=";", index=False)


def traintool_to_amp(input_data_dir, output_data_dir):
    for input_file in os.listdir(input_data_dir):
        input_file_path = os.path.join(input_data_dir, input_file)
        data = pd.read_csv(input_file_path, sep=";")
        data.rename(columns={"sequence": "text", "class": "labels"}, inplace=True)
        data["text"] = data["text"].apply(lambda x: " ".join(list(x)))
        data.to_csv(output_data_dir + input_file, sep=",", index=False)


if __name__ == "__main__":
    amp_data_dir = "../my_data/oliver_ampdeep_transformed/"
    traintool_data_dir = "../my_data/oliver/"

    traintool_to_amp(traintool_data_dir, amp_data_dir)

    print("Done")
